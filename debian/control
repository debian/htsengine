Source: htsengine
Section: sound
Priority: optional
Maintainer: Debian TTS Team <tts-project@alioth-lists.debian.net>
Uploaders: HIGUCHI Daisuke (VDR dai) <dai@debian.org>
Build-Depends: debhelper-compat (= 13),
               docbook-to-man
Standards-Version: 4.6.2
Homepage: https://hts-engine.sourceforge.net/
Vcs-Git: https://salsa.debian.org/tts-team/htsengine.git
Vcs-Browser: https://salsa.debian.org/tts-team/htsengine
Rules-Requires-Root: no

Package: libhtsengine1
Section: libs
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: HMM-based speech synthesis engine API
 This engine is software to synthesize speech waveform from Hidden Markov
 Models (HMM) trained by the HMM-based speech synthesis system (a.k.a. HTS).
 .
 This package contains shared libraries of HTS engine.

Package: libhtsengine-dev
Section: libdevel
Architecture: any
Depends: libhtsengine1 (= ${binary:Version}),
         ${misc:Depends}
Multi-Arch: same
Description: Development files for HMM-based speech synthesis engine API
 This engine is software to synthesize speech waveform from Hidden Markov
 Models (HMM) trained by the HMM-based speech synthesis system (a.k.a. HTS).
 .
 This package contains the development libraries and header files needed by
 programs that want to compile with HTS engine.

Package: htsengine
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: frontend of HMM-based speech synthesis engine
 This engine is software to synthesize speech waveform from Hidden Markov
 Models (HMM) trained by the HMM-based speech synthesis system (a.k.a. HTS).
 .
 This package is a frontend of HTS engine.
